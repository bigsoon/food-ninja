import React, { useState } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';

import IngredientSearchDialog from './IngredientSearchDialog';

export const FoodSearch = () => {
  const [isDialogOpen, toggleDialog] = useState(false);

  return (
    <>
      <Grid container alignItems="center" spacing={40}>
        <Grid item>
          <Typography variant="h3">Food search</Typography>
          <Typography variant="subtitle1" paragraph>
            Search edamam API to find ingredients or recipes for your perfect
            diet plan!
          </Typography>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={() => toggleDialog(true)}
          >
            Search for ingredients
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => toggleDialog(true)}
          >
            Search for recipes
          </Button>
        </Grid>
      </Grid>
      <IngredientSearchDialog open={isDialogOpen} onToggle={toggleDialog} />
    </>
  );
};

export default FoodSearch;
