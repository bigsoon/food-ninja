import React, { useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  Button,
  Fab,
  Grid
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { bool, func } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../firebase';

import IngredientCard from '../food-storage/ingredient/IngredientCard';
import * as IngredientApi from '../food-storage/api';
import { parseIngredientData } from '../food-storage/utils';

const initialValues = {
  name: ''
};

const IngredientSearchSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, 'Too Short!')
    .max(30, 'Too Long!')
    .required('Required')
});

const IngredientSearchDialog = ({ open, onToggle, enqueueSnackbar }) => {
  const [ingredients, setIngredients] = useState(null);

  const handleAddIngredient = (ingredient) => {
    db.collection('ingredients')
      .add(ingredient)
      .then(() =>
        enqueueSnackbar(`${ingredient.name} added!`, {
          variant: 'success'
        })
      );
  };

  const handleFormSubmit = async ({ name }) => {
    try {
      const { data } = await IngredientApi.fetchPossibleIngredientsByName(name);
      setIngredients(parseIngredientData(data));
    } catch (e) {
      enqueueSnackbar('Could not execute search, please try again later.', {
        variant: 'error'
      });
    }
  };
  const renderIngredientSearchDialog = () => {
    return (
      <Dialog
        open={open}
        onClose={() => onToggle(false)}
        aria-labelledby="search-ingredients-dialog"
      >
        <Form>
          <DialogTitle id="search-ingredients-dialog-title">
            Search for ingredients
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Type ingredient name and list of possible results will show.
              Clicking on add button will add ingredient to your storage.
            </DialogContentText>
            <Field
              name="name"
              render={({ field }) => (
                <TextField
                  autoFocus
                  fullWidth
                  label="Ingredient name"
                  placeholder="e.g. apple"
                  margin="normal"
                  {...field}
                />
              )}
            />
            <ErrorMessage name="name">{(msg) => <div>{msg}</div>}</ErrorMessage>
            {ingredients && (
              <div style={{ overflowY: 'scroll', maxHeight: 400 }}>
                {ingredients.map((ingredient, index) => (
                  <Grid
                    container
                    justify="space-evenly"
                    alignItems="center"
                    key={index}
                    style={{ marginBottom: 16 }}
                  >
                    <Grid item>
                      <IngredientCard {...ingredient} />
                    </Grid>
                    <Grid item>
                      <Fab
                        color="secondary"
                        aria-label="Add ingredient"
                        onClick={() => handleAddIngredient(ingredient)}
                      >
                        <AddIcon />
                      </Fab>
                    </Grid>
                  </Grid>
                ))}
              </div>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={() => onToggle(false)} color="primary">
              Cancel
            </Button>
            <Button type="submit" color="primary" variant="contained">
              Search
            </Button>
          </DialogActions>
        </Form>
      </Dialog>
    );
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={IngredientSearchSchema}
      onSubmit={handleFormSubmit}
      render={renderIngredientSearchDialog}
    />
  );
};

IngredientSearchDialog.propTypes = {
  open: bool.isRequired,
  onToggle: func.isRequired,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(IngredientSearchDialog);
