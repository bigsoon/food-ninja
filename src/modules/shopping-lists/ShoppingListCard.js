import React from 'react';
import { string, array, func } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = () => ({
  card: {},
  media: {},
  actions: {
    display: 'flex'
  }
});
const ShoppingListCard = ({ name, items, onEdit, onDelete }) => {
  const getCheckedItems = () => {
    return items.reduce((acc, { checked }) => (checked ? (acc += 1) : acc), 0);
  };
  const getWeight = () => {
    const weight = items.reduce((acc, { amount }) => (acc += amount), 0);
    return ` ${(weight / 1000).toFixed(2)} kg`;
  };

  return (
    <Card>
      <CardHeader
        action={
          <>
            <IconButton onClick={onEdit}>
              <EditIcon />
            </IconButton>
            <IconButton onClick={onDelete}>
              <DeleteIcon />
            </IconButton>
          </>
        }
        title={name || 'Shopping list name'}
      />
      <CardActionArea>
        <CardContent>
          <Typography variant="subtitle1">
            You have bought {getCheckedItems()} of {items.length} items.
          </Typography>
          <Typography variant="subtitle2">
            All the items weighs: {getWeight()}.
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

ShoppingListCard.propTypes = {
  name: string,
  items: array,
  onEdit: func,
  onDelete: func
};

export default withStyles(styles)(ShoppingListCard);
