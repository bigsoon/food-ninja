import React, { useState } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { object } from 'prop-types';

import FirestoreCollection from '../firebase/FirestoreCollection';

import ShoppingListAddDialog from './ShoppingListAddDialog';
import ShoppingListsList from './ShoppingListsList';

const styles = () => ({});

export const ShoppingLists = () => {
  const [isDialogOpen, toggleDialog] = useState(false);

  return (
    <>
      <Grid container alignItems="center" spacing={40}>
        <Grid item>
          <Typography variant="h3">Shopping list</Typography>
          <Typography variant="subtitle1" paragraph>
            Create your perfect shopping list.
          </Typography>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={() => toggleDialog(true)}
          >
            Add shopping list
          </Button>
        </Grid>
      </Grid>
      <FirestoreCollection
        name="ingredients"
        render={(ingredients) => (
          <FirestoreCollection
            name="recipes"
            render={(recipes) => (
              <ShoppingListAddDialog
                open={isDialogOpen}
                recipes={recipes}
                ingredients={ingredients}
                onClose={() => toggleDialog(false)}
              />
            )}
          />
        )}
      />
      <FirestoreCollection
        name="shopping-lists"
        component={<ShoppingListsList />}
      />
    </>
  );
};

ShoppingLists.propTypes = {
  classes: object.isRequired
};

export default withStyles(styles, { withTheme: true })(ShoppingLists);
