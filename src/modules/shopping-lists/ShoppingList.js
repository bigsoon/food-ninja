import React from 'react';
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  TextField,
  IconButton,
  Checkbox,
  Paper,
  Typography
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { Field } from 'formik';
import PropTypes from 'prop-types';

const styles = (theme) => ({
  listItem: {},
  listItemChecked: {
    textDecoration: 'line-through',
    opacity: 0.2
  }
});

const ShoppingList = ({
  items,
  arrayHelpers,
  classes: { listItem, listItemChecked }
}) => {
  const getCheckedItems = () => {
    return items.reduce((acc, { checked }) => (checked ? (acc += 1) : acc), 0);
  };
  const getWeight = () => {
    const weight = items.reduce((acc, { amount }) => (acc += amount), 0);
    return ` ${(weight / 1000).toFixed(2)} kg`;
  };
  return (
    <List>
      <Typography variant="subtitle1" gutterBottom>
        You have bought {getCheckedItems(items)} of {items.length} items.
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        All the items weighs:
        {getWeight(items)}
      </Typography>
      <Grid container spacing={16}>
        {items
          .sort((a, b) => a.checked - b.checked)
          .map(({ id, name, price, checked }, index) => (
            <Grid item xs={12} sm={12} md={6} lg={4} key={id}>
              <Paper className={checked ? listItemChecked : listItem}>
                <ListItem>
                  <Field
                    name={`items[${index}].checked`}
                    type="checkbox"
                    render={({ field }) => (
                      <Checkbox
                        color="secondary"
                        checked={items[index].checked}
                        disableRipple
                        {...field}
                      />
                    )}
                  />
                  <ListItemText primary={name} secondary={`${price} PLN`} />
                  <ListItemSecondaryAction>
                    <Grid container alignItems="center" spacing={16}>
                      <Grid item>
                        <Field
                          name={`items[${index}].amount`}
                          render={({ field }) => (
                            <TextField
                              label="Amount (g)"
                              placeholder="100"
                              variant="outlined"
                              type="number"
                              style={{ width: 100 }}
                              {...field}
                            />
                          )}
                        />
                      </Grid>
                      <Grid item>
                        <IconButton
                          color="secondary"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          <Delete />
                        </IconButton>
                      </Grid>
                    </Grid>
                  </ListItemSecondaryAction>
                </ListItem>
              </Paper>
            </Grid>
          ))}
      </Grid>
    </List>
  );
};

ShoppingList.propTypes = {
  items: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  arrayHelpers: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(ShoppingList);
