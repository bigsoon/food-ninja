import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import { Grid, TextField } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Formik, Form, FieldArray, Field, ErrorMessage } from 'formik';
import { withSnackbar } from 'notistack';

import { AutosuggestField } from '../../components/forms';
import { db } from '../firebase';

import ShoppingList from './ShoppingList';

const styles = {
  appBar: {
    position: 'relative'
  },
  flex: {
    flex: 1
  },
  container: {
    margin: 24
  },
  listItem: {
    border: `1px solid #FAFAFA`,
    padding: 32
  }
};

const initialValues = {
  name: '',
  items: []
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

export const ShoppingListAddDialog = ({
  classes,
  open,
  recipes,
  ingredients,
  enqueueSnackbar,
  onClose
}) => {
  const handleFormSubmit = (values) => {
    db.collection('shopping-lists')
      .add(values)
      .then(() =>
        enqueueSnackbar(`${values.name} shopping list added!`, {
          variant: 'success'
        })
      );
    onClose();
  };
  const handleIngredientAdd = (items, item, arrayHelpers) => {
    const ingredient = { ...item, checked: false };
    const duplicatedIngredientIndex = items.findIndex(
      (item) => item.id === ingredient.id
    );
    const duplicatedIngredient = items[duplicatedIngredientIndex];

    if (duplicatedIngredient) {
      return replaceIngredientAmount(
        arrayHelpers,
        duplicatedIngredientIndex,
        ingredient,
        duplicatedIngredient
      );
    }

    return arrayHelpers.push(ingredient);
  };

  const handleRecipeAdd = (items, { ingredients }, arrayHelpers) => {
    return ingredients.map((item) => {
      const ingredient = { ...item, checked: false };
      const duplicatedIngredientIndex = items.findIndex(
        (item) => item.id === ingredient.id
      );
      const duplicatedIngredient = items[duplicatedIngredientIndex];
      if (duplicatedIngredient) {
        return replaceIngredientAmount(
          arrayHelpers,
          duplicatedIngredientIndex,
          ingredient,
          duplicatedIngredient
        );
      }

      return arrayHelpers.push(ingredient);
    });
  };
  const replaceIngredientAmount = (
    arrayHelpers,
    duplicatedIngredientIndex,
    ingredient,
    duplicatedIngredient
  ) => {
    return arrayHelpers.replace(duplicatedIngredientIndex, {
      ...ingredient,
      amount: ingredient.amount + duplicatedIngredient.amount
    });
  };
  const renderForm = ({ values: { items } }) => {
    return (
      <Form>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={onClose} aria-label="Close">
              <Close />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              Add shopping list
            </Typography>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              size="large"
            >
              save
            </Button>
          </Toolbar>
        </AppBar>
        <div className={classes.container}>
          <Field
            name="name"
            render={({ field }) => (
              <TextField
                fullWidth
                label="Name"
                placeholder="Shopping list name"
                variant="outlined"
                margin="normal"
                {...field}
              />
            )}
          />
          <ErrorMessage name="directions">
            {(msg) => <div>{msg}</div>}
          </ErrorMessage>
          <FieldArray
            name="items"
            render={(arrayHelpers) => (
              <>
                <Grid container spacing={16}>
                  <Grid item xs={12} md={6}>
                    <AutosuggestField
                      data={ingredients}
                      name="suggest-ingredient"
                      label="Add ingredients"
                      onSelect={(item) =>
                        handleIngredientAdd(items, item, arrayHelpers)
                      }
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <AutosuggestField
                      data={recipes}
                      name="suggest-recipe"
                      label="Add recipes"
                      onSelect={(item) =>
                        handleRecipeAdd(items, item, arrayHelpers)
                      }
                    />
                  </Grid>
                </Grid>
                {items.length > 0 ? (
                  <ShoppingList items={items} arrayHelpers={arrayHelpers} />
                ) : (
                  <Typography align="center">
                    You have no items added to this list.
                  </Typography>
                )}
              </>
            )}
          />
        </div>
      </Form>
    );
  };
  return (
    <Dialog
      fullScreen
      open={open}
      onClose={onClose}
      TransitionComponent={Transition}
    >
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        render={renderForm}
      />
      ;
    </Dialog>
  );
};

ShoppingListAddDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  recipes: PropTypes.array.isRequired,
  ingredients: PropTypes.array.isRequired
};

export default withSnackbar(withStyles(styles)(ShoppingListAddDialog));
