import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { array, func } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../firebase';
import FirestoreCollection from '../firebase/FirestoreCollection';

import ShoppingListCard from './ShoppingListCard';
import ShoppingListEditDialog from './ShoppingListEditDialog';

const ShoppingListsList = ({ data, enqueueSnackbar }) => {
  const [open, toggleOpen] = useState(false);
  const [initialValues, setInitialValues] = useState(null);

  const renderShoppingListsCards = () =>
    data.map(({ id, ...shoppingList }) => (
      <Grid item xs={12} md={4} key={id}>
        <ShoppingListCard
          {...shoppingList}
          onEdit={() => {
            setInitialValues(
              data.find((shoppingList) => shoppingList.id === id)
            );
            toggleOpen(true);
          }}
          onDelete={() => {
            db.collection('shopping-lists')
              .doc(id)
              .delete()
              .then(() =>
                enqueueSnackbar(`${shoppingList.name} deleted!`, {
                  variant: 'success'
                })
              );
          }}
        />
      </Grid>
    ));
  return (
    <>
      <Grid container spacing={24}>
        {renderShoppingListsCards()}
      </Grid>
      <FirestoreCollection
        name="ingredients"
        render={(ingredients) => (
          <FirestoreCollection
            name="recipes"
            render={(recipes) => (
              <ShoppingListEditDialog
                initialValues={initialValues}
                ingredients={ingredients}
                recipes={recipes}
                open={open}
                onClose={() => toggleOpen(false)}
              />
            )}
          />
        )}
      />
    </>
  );
};

ShoppingListsList.propTypes = {
  data: array,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(ShoppingListsList);
