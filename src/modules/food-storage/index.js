import React, { useState } from 'react';
import { Grid, Typography, Button, Tabs, Tab } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { object } from 'prop-types';

import FirestoreCollection from '../firebase/FirestoreCollection';
import IngredientList from './ingredient/IngredientList';
import IngredientAddDrawer from './ingredient/IngredientAddDrawer';
import RecipeList from './recipe/RecipeList';
import RecipeAddDrawer from './recipe/RecipeAddDrawer';

const styles = (theme) => ({
  tabs: {
    marginBottom: theme.spacing.unit * 3
  }
});

export const FoodStorage = ({ classes: { tabs } }) => {
  const [isAddIngredientDrawerOpen, toggleAddIngredientDrawer] = useState(
    false
  );
  const [isAddRecipeDrawerOpen, toggleAddRecipeDrawer] = useState(false);
  const [tabId, setTabId] = useState(0);

  return (
    <>
      <Grid container alignItems="center" spacing={40}>
        <Grid item>
          <Typography variant="h3">Food storage</Typography>
          <Typography variant="subtitle1" paragraph>
            Create ingredients and recipes to put in your food plan.
          </Typography>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={() => toggleAddIngredientDrawer(true)}
          >
            Add ingredient
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => toggleAddRecipeDrawer(true)}
          >
            Add recipe
          </Button>
        </Grid>
      </Grid>
      <Tabs
        value={tabId}
        textColor="secondary"
        className={tabs}
        onChange={(e, v) => setTabId(v)}
      >
        <Tab label="Ingredients" />
        <Tab label="Recipes" />
      </Tabs>
      {tabId === 0 && (
        <FirestoreCollection
          name="ingredients"
          component={<IngredientList />}
        />
      )}
      {tabId === 1 && (
        <FirestoreCollection name="recipes" component={<RecipeList />} />
      )}
      <IngredientAddDrawer
        open={isAddIngredientDrawerOpen}
        onToggle={toggleAddIngredientDrawer}
      />
      <FirestoreCollection
        name="ingredients"
        component={
          <RecipeAddDrawer
            open={isAddRecipeDrawerOpen}
            onToggle={toggleAddRecipeDrawer}
          />
        }
      />
    </>
  );
};

FoodStorage.propTypes = {
  classes: object.isRequired
};

export default withStyles(styles, { withTheme: true })(FoodStorage);
