import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { array, func } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../../firebase';
import FirestoreCollection from '../../firebase/FirestoreCollection';

import RecipeCard from './RecipeCard';
import RecipeEditDrawer from './RecipeEditDrawer';

const RecipeList = ({ data, enqueueSnackbar }) => {
  const [open, toggleOpen] = useState(false);
  const [initialValues, setInitialValues] = useState(null);

  const renderRecipeCards = () =>
    data.map(({ id, ...recipe }) => (
      <Grid item xs={4} key={id}>
        <RecipeCard
          {...recipe}
          editable
          onEdit={() => {
            setInitialValues(data.find((recipe) => recipe.id === id));
            toggleOpen(true);
          }}
          onDelete={() => {
            db.collection('recipes')
              .doc(id)
              .delete()
              .then(() =>
                enqueueSnackbar(`${recipe.name} deleted!`, {
                  variant: 'success'
                })
              );
          }}
        />
      </Grid>
    ));
  return (
    <>
      <Grid container spacing={24}>
        {renderRecipeCards()}
      </Grid>
      <FirestoreCollection
        name="ingredients"
        component={
          <RecipeEditDrawer
            initialValues={initialValues}
            open={open}
            onToggle={toggleOpen}
          />
        }
      />
    </>
  );
};

RecipeList.propTypes = {
  data: array,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(RecipeList);
