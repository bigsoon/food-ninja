import React from 'react';
import { string, number, object, bool, func } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import CaloriesChart from '../CaloriesChart';

const styles = () => ({
  card: {},
  media: {},
  actions: {
    display: 'flex'
  }
});
const RecipeCard = ({
  name,
  desc,
  imageUrl,
  carbs,
  protein,
  fat,
  fibre,
  kcal,
  editable = false,
  classes,
  onEdit,
  onDelete
}) => (
  <Card className={classes.card}>
    <CardHeader
      action={
        editable ? (
          <>
            <IconButton onClick={onEdit}>
              <EditIcon />
            </IconButton>
            <IconButton onClick={onDelete}>
              <DeleteIcon />
            </IconButton>
          </>
        ) : null
      }
      title={name || 'Recipe name'}
      subheader={desc || 'Recipe description.'}
    />
    {imageUrl && (
      <CardMedia
        src={imageUrl}
        component="img"
        alt={`${name} recipe image`}
        className={classes.media}
      />
    )}
    <CardActionArea>
      <CardContent>
        <Grid container justify="center">
          <CaloriesChart
            carbs={carbs}
            fat={fat}
            protein={protein}
            fibre={fibre}
          />
        </Grid>
      </CardContent>
    </CardActionArea>
    <CardActions className={classes.actions} disableActionSpacing>
      <Grid container justify="space-between" alignItems="center">
        <IconButton aria-label="Add to favorites">
          <FavoriteIcon />
        </IconButton>
        <Chip label={`${Math.floor(kcal)} calories`} color="secondary" />
      </Grid>
    </CardActions>
  </Card>
);

RecipeCard.propTypes = {
  name: string,
  desc: string,
  imageUrl: string,
  carbs: number.isRequired,
  fat: number.isRequired,
  protein: number.isRequired,
  fibre: number.isRequired,
  kcal: number,
  editable: bool,
  classes: object.isRequired,
  onEdit: func,
  onDelete: func
};

export default withStyles(styles)(RecipeCard);
