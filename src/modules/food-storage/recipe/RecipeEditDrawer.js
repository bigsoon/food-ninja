import React from 'react';
import { Formik, Form, FieldArray, Field, ErrorMessage } from 'formik';
import {
  Grid,
  Drawer,
  Typography,
  TextField,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  Avatar
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { bool, func, object, array } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../../firebase';
import { AutosuggestField } from '../../../components/forms';
import {
  calculateIngredientNutrients,
  calculateRecipeNutrients
} from '../utils';
import { RecipeSchema } from '../validation';

const styles = (theme) => ({
  container: {
    width: 450,
    margin: theme.spacing.unit * 3
  },
  buttonGroup: {
    marginTop: theme.spacing.unit * 3
  },
  preview: {
    margin: `${theme.spacing.unit * 2} 0`
  },
  error: {
    color: 'red'
  }
});

const RecipeEditDrawer = ({
  initialValues,
  data,
  open,
  classes: { container, buttonGroup, preview, error },
  onToggle,
  enqueueSnackbar
}) => {
  const handleFormSubmit = ({ id, ...values }) => {
    db.collection('recipes')
      .doc(id)
      .update({ ...values, ...calculateRecipeNutrients(values.ingredients) })
      .then(() =>
        enqueueSnackbar(`${values.name} recipe edited!`, {
          variant: 'success'
        })
      );
    onToggle(false);
  };
  const handleAmountChange = (arrayHelpers, ingredient, index) => ({
    target: { value }
  }) => {
    const originalIngredient = data.find(
      ({ name }) => name === ingredient.name
    );

    arrayHelpers.replace(index, {
      ...ingredient,
      ...calculateIngredientNutrients({
        ...originalIngredient,
        amount: value ? Number(value) : 1
      })
    });
  };
  const renderForm = ({ values }) => {
    return (
      <Form className={container}>
        <Typography variant="h4" gutterBottom>
          Edit recipe
        </Typography>
        <Field
          name="name"
          render={({ field }) => (
            <TextField
              fullWidth
              label="Name"
              placeholder="Recipe name"
              variant="outlined"
              margin="normal"
              {...field}
            />
          )}
        />
        <ErrorMessage name="name">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <Field
          name="desc"
          render={({ field }) => (
            <TextField
              fullWidth
              label="Description"
              placeholder="Recipe description"
              variant="outlined"
              margin="normal"
              {...field}
            />
          )}
        />
        <ErrorMessage name="desc">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <Field
          name="imageUrl"
          render={({ field }) => (
            <TextField
              fullWidth
              label="Image URL"
              placeholder="Recipe image"
              variant="outlined"
              margin="normal"
              {...field}
            />
          )}
        />
        <ErrorMessage name="imageUrl">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <Field
          name="directions"
          render={({ field }) => (
            <TextField
              fullWidth
              multiline
              label="Directions"
              placeholder="Steps for preparing recipe"
              variant="outlined"
              margin="normal"
              rows={5}
              {...field}
            />
          )}
        />
        <ErrorMessage name="directions">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <FieldArray
          name="ingredients"
          render={(arrayHelpers) => (
            <>
              <AutosuggestField
                data={data}
                name="suggest-ingredient"
                label="Add ingredient"
                onSelect={(ingredient) => arrayHelpers.push(ingredient)}
              />
              {values.ingredients.length > 0 ? (
                values.ingredients.map(({ name, kcal, price }, index) => (
                  <List key={index}>
                    <ListItem>
                      <ListItemText
                        primary={name}
                        secondary={`${kcal} calories, ${price} PLN`}
                      />
                      <ListItemSecondaryAction>
                        <Grid container alignItems="center" spacing={16}>
                          <Grid item>
                            <TextField
                              label="Amount (g)"
                              placeholder="100"
                              variant="outlined"
                              margin="normal"
                              type="number"
                              value={values.ingredients[index].amount}
                              onChange={handleAmountChange(
                                arrayHelpers,
                                values.ingredients[index],
                                index
                              )}
                              style={{ width: 120 }}
                            />
                          </Grid>
                          <Grid item>
                            <IconButton
                              color="secondary"
                              onClick={() => arrayHelpers.remove(index)}
                            >
                              <Delete />
                            </IconButton>
                          </Grid>
                        </Grid>
                      </ListItemSecondaryAction>
                    </ListItem>
                  </List>
                ))
              ) : (
                <Typography align="center">
                  You have no ingredients added to this recipe.
                </Typography>
              )}
            </>
          )}
        />
        <ErrorMessage name="ingredients">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <Grid container justify="center" className={preview} />
        <div className={buttonGroup}>
          <Button color="primary" size="large" onClick={() => onToggle(false)}>
            Cancel
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="large"
          >
            Edit
          </Button>
        </div>
      </Form>
    );
  };

  return (
    <Drawer anchor="right" open={open} onClose={() => onToggle(false)}>
      <div>
        <Formik
          initialValues={initialValues}
          validationSchema={RecipeSchema}
          onSubmit={handleFormSubmit}
          render={renderForm}
        />
      </div>
    </Drawer>
  );
};

RecipeEditDrawer.propTypes = {
  initialValues: object,
  data: array.isRequired,
  open: bool.isRequired,
  classes: object.isRequired,
  onToggle: func.isRequired,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(
  withStyles(styles, { withTheme: true })(RecipeEditDrawer)
);
