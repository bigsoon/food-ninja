import React from 'react';
import {
  Grid,
  Drawer,
  Typography,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  Avatar
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { bool, func, object, array } from 'prop-types';
import { withSnackbar } from 'notistack';

import {
  ABForm,
  Form,
  AutosuggestField,
  TextField,
  ErrorMessage,
  FieldArray
} from '../../../components/forms';
import { db } from '../../firebase';
import {
  calculateIngredientNutrients,
  calculateRecipeNutrients
} from '../utils';
import { RecipeSchema } from '../validation';

const styles = (theme) => ({
  buttonGroup: {
    marginTop: theme.spacing.unit * 3
  }
});

const initialValues = {
  name: '',
  desc: '',
  imageUrl: '',
  directions: '',
  ingredients: [],
  totalTime: 60
};

const AddRecipeDrawer = ({
  data,
  open,
  classes: { buttonGroup, preview },
  onToggle,
  enqueueSnackbar
}) => {
  const handleFormSubmit = (values) => {
    db.collection('recipes')
      .add({
        ...values,
        ...calculateRecipeNutrients(values.ingredients),
        type: 'RECIPE'
      })
      .then(() =>
        enqueueSnackbar(`${values.name} recipe added!`, {
          variant: 'success'
        })
      );
    onToggle(false);
  };
  const handleAmountChange = (arrayHelpers, ingredient, index) => ({
    target: { value }
  }) => {
    const originalIngredient = data.find(
      ({ name }) => name === ingredient.name
    );

    arrayHelpers.replace(index, {
      ...ingredient,
      ...calculateIngredientNutrients({
        ...originalIngredient,
        amount: value ? Number(value) : 1
      })
    });
  };
  const renderForm = ({ values: { ingredients } }) => {
    return (
      <Form>
        <Typography variant="h4" gutterBottom>
          Add recipe
        </Typography>
        <TextField
          name="name"
          fullWidth
          label="Name"
          placeholder="Recipe name"
        />
        <ErrorMessage name="name" />
        <TextField
          name="desc"
          fullWidth
          label="Description"
          placeholder="Recipe description"
        />
        <ErrorMessage name="desc" />
        <TextField
          name="imageUrl"
          fullWidth
          label="Image URL"
          placeholder="Recipe image"
        />
        <ErrorMessage name="imageUrl" />
        <TextField
          name="directions"
          fullWidth
          multiline
          label="Directions"
          placeholder="Steps for preparing recipe"
          rows={5}
        />
        <ErrorMessage name="directions" />
        <FieldArray
          name="ingredients"
          render={(arrayHelpers) => (
            <>
              <AutosuggestField
                name="suggest-ingredient"
                data={data}
                label="Add ingredient"
                onSelect={(ingredient) => arrayHelpers.push(ingredient)}
              />
              {ingredients.length > 0 ? (
                <List>
                  {ingredients.map(({ name, kcal, price }, index) => (
                    <ListItem key={index}>
                      <ListItemText
                        primary={name}
                        secondary={`${kcal} calories, ${price} PLN`}
                      />
                      <ListItemSecondaryAction>
                        <Grid container alignItems="center" spacing={16}>
                          <Grid item>
                            <TextField
                              name={`ingredients[${index}]`}
                              label="Amount (g)"
                              placeholder="100"
                              type="number"
                              value={ingredients[index].amount}
                              onChange={handleAmountChange(
                                arrayHelpers,
                                ingredients[index],
                                index
                              )}
                              style={{ width: 120 }}
                            />
                          </Grid>
                          <Grid item>
                            <IconButton
                              color="secondary"
                              onClick={() => arrayHelpers.remove(index)}
                            >
                              <Delete />
                            </IconButton>
                          </Grid>
                        </Grid>
                      </ListItemSecondaryAction>
                    </ListItem>
                  ))}
                </List>
              ) : (
                <Typography align="center">
                  You have no ingredients added to this recipe.
                </Typography>
              )}
            </>
          )}
        />
        <ErrorMessage name="ingredients" />
        <div className={buttonGroup}>
          <Button color="primary" size="large" onClick={() => onToggle(false)}>
            Cancel
          </Button>
          <Button
            type="submit"
            color="primary"
            variant="contained"
            size="large"
          >
            Add
          </Button>
        </div>
      </Form>
    );
  };

  return (
    <Drawer anchor="right" open={open} onClose={() => onToggle(false)}>
      <ABForm
        initialValues={initialValues}
        validationSchema={RecipeSchema}
        onSubmit={handleFormSubmit}
        render={renderForm}
      />
    </Drawer>
  );
};

AddRecipeDrawer.propTypes = {
  data: array.isRequired,
  open: bool.isRequired,
  classes: object.isRequired,
  onToggle: func.isRequired,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(
  withStyles(styles, { withTheme: true })(AddRecipeDrawer)
);
