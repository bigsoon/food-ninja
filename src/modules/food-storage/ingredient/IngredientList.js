import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { array, func } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../../firebase';

import IngredientCard from './IngredientCard';
import EditIngredientDrawer from './IngredientEditDrawer';

const IngredientList = ({ data, enqueueSnackbar }) => {
  const [open, toggleOpen] = useState(false);
  const [initialValues, setInitialValues] = useState(null);

  const renderIngredientCards = () =>
    data.map(({ id, ...ingredient }) => (
      <Grid item xs={12} sm={6} lg={3} key={id}>
        <IngredientCard
          {...ingredient}
          editable
          onEdit={() => {
            setInitialValues(data.find((ingredient) => ingredient.id === id));
            toggleOpen(true);
          }}
          onDelete={() => {
            db.collection('ingredients')
              .doc(id)
              .delete()
              .then(() =>
                enqueueSnackbar(`${ingredient.name} deleted!`, {
                  variant: 'success'
                })
              );
          }}
        />
      </Grid>
    ));
  return (
    <>
      <Grid container spacing={24}>
        {renderIngredientCards()}
      </Grid>
      <EditIngredientDrawer
        initialValues={initialValues}
        open={open}
        onToggle={toggleOpen}
      />
    </>
  );
};

IngredientList.propTypes = {
  data: array,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(IngredientList);
