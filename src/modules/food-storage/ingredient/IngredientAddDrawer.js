import React from 'react';
import { Grid, Drawer, Typography, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { bool, func, object } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../../firebase';
import {
  ABForm,
  Form,
  TextField,
  ErrorMessage
} from '../../../components/forms';
import { calculateIngredientNutrients } from '../utils';
import { IngredientSchema } from '../validation';

const styles = (theme) => ({
  buttonGroup: {
    marginTop: theme.spacing.unit * 3
  }
});

const initialValues = {
  name: '',
  desc: '',
  carbs: 0,
  protein: 0,
  fat: 0,
  fibre: 0,
  amount: 100,
  price: 0,
  unit: 'g'
};

const AddIngredientDrawer = ({
  open,
  classes: { buttonGroup, preview },
  onToggle,
  enqueueSnackbar
}) => {
  const handleFormSubmit = (values) => {
    db.collection('ingredients')
      .add({
        ...values,
        ...calculateIngredientNutrients(values),
        type: 'INGREDIENT'
      })
      .then(() =>
        enqueueSnackbar(`${values.name} ingredient added!`, {
          variant: 'success'
        })
      );
    onToggle(false);
  };
  const renderForm = () => {
    return (
      <Form>
        <Typography variant="h4" gutterBottom>
          Add ingredient
        </Typography>
        <TextField
          name="name"
          label="Name"
          placeholder="Ingredient name"
          fullWidth
        />
        <ErrorMessage name="name" />
        <TextField
          name="desc"
          label="Description"
          placeholder="Ingredient description"
          fullWidth
        />
        <ErrorMessage name="desc" />
        <Grid container spacing={16}>
          <Grid item xs={6}>
            <TextField
              name="carbs"
              label="Carbohydrates (100g)"
              type="number"
              fullWidth
            />
            <ErrorMessage name="carbs" />
          </Grid>
          <Grid item xs={6}>
            <TextField
              name="protein"
              label="Protein (100g)"
              type="number"
              fullWidth
            />
            <ErrorMessage name="protein" />
          </Grid>
          <Grid item xs={6}>
            <TextField name="fat" label="Fat (100g)" type="number" fullWidth />
            <ErrorMessage name="fat" />
          </Grid>
          <Grid item xs={6}>
            <TextField
              name="fibre"
              label="Fibre (100g)"
              type="number"
              fullWidth
            />
            <ErrorMessage name="fibre" />
          </Grid>
        </Grid>
        <TextField
          name="price"
          fullWidth
          label="Price"
          placeholder="Ingredient price"
        />
        <ErrorMessage name="price" />
        <div className={buttonGroup}>
          <Button color="primary" size="large" onClick={() => onToggle(false)}>
            Cancel
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="large"
          >
            Add
          </Button>
        </div>
      </Form>
    );
  };

  return (
    <Drawer anchor="right" open={open} onClose={() => onToggle(false)}>
      <ABForm
        initialValues={initialValues}
        validationSchema={IngredientSchema}
        onSubmit={handleFormSubmit}
        render={renderForm}
      />
    </Drawer>
  );
};

AddIngredientDrawer.propTypes = {
  open: bool.isRequired,
  classes: object.isRequired,
  onToggle: func.isRequired,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(
  withStyles(styles, { withTheme: true })(AddIngredientDrawer)
);
