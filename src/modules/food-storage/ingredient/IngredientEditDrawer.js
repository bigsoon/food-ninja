import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { Grid, Drawer, Typography, TextField, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { bool, func, object } from 'prop-types';
import { withSnackbar } from 'notistack';

import { db } from '../../firebase';

import { calculateIngredientNutrients } from '../utils';
import { IngredientSchema } from '../validation';

const styles = (theme) => ({
  container: {
    width: 450,
    margin: theme.spacing.unit * 3
  },
  buttonGroup: {
    marginTop: theme.spacing.unit * 3
  },
  preview: {
    margin: `${theme.spacing.unit * 2} 0`
  },
  error: {
    color: 'red'
  }
});

const EditIngredientDrawer = ({
  initialValues,
  open,
  classes: { container, buttonGroup, preview, error },
  onToggle,
  enqueueSnackbar
}) => {
  const handleFormSubmit = ({ id, ...values }) => {
    db.collection('ingredients')
      .doc(id)
      .update({ ...values, ...calculateIngredientNutrients(values) })
      .then(() =>
        enqueueSnackbar(`${values.name} ingredient edited!`, {
          variant: 'success'
        })
      );
    onToggle(false);
  };
  const renderForm = ({ values }) => {
    return (
      <Form className={container}>
        <Typography variant="h4" gutterBottom>
          Edit ingredient
        </Typography>
        <Field
          name="name"
          render={({ field }) => (
            <TextField
              fullWidth
              label="Name"
              placeholder="Name your ingredient"
              variant="outlined"
              margin="normal"
              {...field}
            />
          )}
        />
        <ErrorMessage name="name">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <Field
          name="desc"
          render={({ field }) => (
            <TextField
              fullWidth
              multiline
              label="Description"
              placeholder="Describe your ingredient"
              variant="outlined"
              margin="normal"
              {...field}
            />
          )}
        />
        <ErrorMessage name="desc">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <Grid container spacing={16}>
          <Grid item xs={6}>
            <Field
              name="carbs"
              render={({ field }) => (
                <TextField
                  label="Carbohydrates (100g)"
                  variant="outlined"
                  margin="normal"
                  type="number"
                  fullWidth
                  {...field}
                />
              )}
            />
            <ErrorMessage name="carbs">
              {(msg) => <div className={error}>{msg}</div>}
            </ErrorMessage>
          </Grid>
          <Grid item xs={6}>
            <Field
              name="protein"
              render={({ field }) => (
                <TextField
                  label="Protein (100g)"
                  variant="outlined"
                  margin="normal"
                  type="number"
                  fullWidth
                  {...field}
                />
              )}
            />
            <ErrorMessage name="protein">
              {(msg) => <div className={error}>{msg}</div>}
            </ErrorMessage>
          </Grid>
          <Grid item xs={6}>
            <Field
              name="fat"
              render={({ field }) => (
                <TextField
                  label="Fat (100g)"
                  variant="outlined"
                  margin="normal"
                  type="number"
                  fullWidth
                  {...field}
                />
              )}
            />
            <ErrorMessage name="fat">
              {(msg) => <div className={error}>{msg}</div>}
            </ErrorMessage>
          </Grid>
          <Grid item xs={6}>
            <Field
              name="fibre"
              render={({ field }) => (
                <TextField
                  label="Fibre (100g)"
                  variant="outlined"
                  margin="normal"
                  type="number"
                  fullWidth
                  {...field}
                />
              )}
            />
            <ErrorMessage name="fibre">
              {(msg) => <div className={error}>{msg}</div>}
            </ErrorMessage>
          </Grid>
        </Grid>
        <Field
          name="price"
          render={({ field }) => (
            <TextField
              fullWidth
              label="Price"
              placeholder="Ingredient price"
              variant="outlined"
              margin="normal"
              {...field}
            />
          )}
        />
        <ErrorMessage name="price">
          {(msg) => <div className={error}>{msg}</div>}
        </ErrorMessage>
        <div className={buttonGroup}>
          <Button
            color="primary"
            size="large"
            onClick={() => {
              onToggle(false);
            }}
          >
            Cancel
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="large"
          >
            Edit
          </Button>
        </div>
      </Form>
    );
  };

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={() => {
        onToggle(false);
      }}
    >
      <div>
        <Formik
          initialValues={initialValues}
          validationSchema={IngredientSchema}
          onSubmit={handleFormSubmit}
          render={renderForm}
        />
      </div>
    </Drawer>
  );
};

EditIngredientDrawer.propTypes = {
  initialValues: object,
  open: bool.isRequired,
  classes: object.isRequired,
  onToggle: func.isRequired,
  enqueueSnackbar: func.isRequired
};

export default withSnackbar(
  withStyles(styles, { withTheme: true })(EditIngredientDrawer)
);
