import * as Yup from 'yup';

export const IngredientSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(30, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(100, 'Too Long!')
    .required('Required'),
  carbs: Yup.number()
    .min(0, 'Invalid number!')
    .max(100, 'Invalid number!'),
  protein: Yup.number()
    .min(0, 'Invalid number!')
    .max(100, 'Invalid number!'),
  fat: Yup.number()
    .min(0, 'Invalid number!')
    .max(100, 'Invalid number!'),
  fibre: Yup.number()
    .min(0, 'Invalid number!')
    .max(100, 'Invalid number!'),
  amount: Yup.number()
    .min(0, 'Invalid number!')
    .required(),
  unit: Yup.string().required()
});

export const RecipeSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(30, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(100, 'Too Long!')
    .required('Required'),
  directions: Yup.string()
    .min(2, 'Too Short!')
    .required(),
  imageUrl: Yup.string().url(),
  ingredients: Yup.array(IngredientSchema)
    .min(2, 'Recipe needs atleast 2 ingredients!')
    .required(),
  totalTime: Yup.number().min(
    1,
    'You are fast indeed, but be realistic, please!'
  )
});
