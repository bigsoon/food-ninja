const NutritionKC = {
  carbs: 4,
  protein: 4,
  fat: 9,
  fibre: 2
};

const calculateValuePerAmount = (value, amount) =>
  parseFloat((value * (amount / 100)).toFixed(1));

export const calculateIngredientNutrients = ({
  carbs = 0,
  protein = 0,
  fat = 0,
  fibre = 0,
  amount = 100
}) => {
  const calculatedCarbs = calculateValuePerAmount(carbs, amount);
  const calculatedProtein = calculateValuePerAmount(protein, amount);
  const calculatedFat = calculateValuePerAmount(fat, amount);
  const calculatedFibre = calculateValuePerAmount(fibre, amount);

  const carbsKC = calculatedCarbs * NutritionKC.carbs;
  const proteinKC = calculatedProtein * NutritionKC.protein;
  const fatKC = calculatedFat * NutritionKC.fat;
  const fibreKC = calculatedFibre * NutritionKC.fibre;
  const ingredientKJ = carbsKC + proteinKC + fatKC + fibreKC;

  return {
    kcal: Math.floor(ingredientKJ),
    carbs: calculatedCarbs,
    protein: calculatedProtein,
    fat: calculatedFat,
    fibre: calculatedFibre,
    amount
  };
};

export const calculateRecipeNutrients = (ingredients) =>
  ingredients.reduce(
    (acc, { kcal, carbs, protein, fat, fibre, amount }) => ({
      kcal: acc.kcal + kcal,
      carbs: acc.carbs + carbs,
      protein: acc.protein + protein,
      fat: acc.fat + fat,
      fibre: acc.fibre + fibre,
      amount: acc.amount + amount
    }),
    { kcal: 0, carbs: 0, protein: 0, fat: 0, fibre: 0, amount: 0 }
  );

export const parseIngredientData = ({ text, hints }) => {
  const temporaryHints = [hints[0]];
  return temporaryHints.map(
    ({
      food: {
        label,
        nutrients: { CHOCDF = 0, PROCNT = 0, FAT = 0, FIBTG = 0, ENERC_KCAL }
      }
    }) => ({
      name: text,
      desc: label,
      carbs: CHOCDF,
      protein: PROCNT,
      fat: FAT,
      fibre: FIBTG,
      kcal: ENERC_KCAL
    })
  );
};
