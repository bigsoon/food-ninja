import axios from 'axios';

const FOOD_API_URL = 'https://api.edamam.com/api/food-database/parser?';

export const fetchPossibleIngredientsByName = (name) =>
  axios.get(
    `${FOOD_API_URL}ingr=${name}&app_id=${
      process.env.EDAMAM_FOOD_API_APP_ID
    }&app_key=${process.env.EDAMAM_FOOD_API_APP_KEY}`
  );
