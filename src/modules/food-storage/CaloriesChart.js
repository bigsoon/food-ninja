import React from 'react';
import { PieChart, Pie, Cell, Tooltip } from 'recharts';

export const CaloriesChart = ({ carbs, fat, protein, fibre }) => (
  <PieChart width={120} height={120}>
    <Pie
      data={[
        { name: 'carbs', value: carbs },
        { name: 'fat', value: fat },
        { name: 'protein', value: protein },
        { name: 'fibre', value: fibre }
      ]}
      cx="50%"
      cy="50%"
      dataKey="value"
      innerRadius={35}
      outerRadius={55}
      paddingAngle={10}
    >
      <Cell fill="green" />
      <Cell fill="yellow" />
      <Cell fill="white" />
      <Cell fill="cyan" />
    </Pie>
    <Tooltip />
  </PieChart>
);

export default CaloriesChart;
