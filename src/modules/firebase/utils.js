export const parseQuerySnapshot = (querySnapshot) => {
  const items = [];

  querySnapshot.forEach((doc) => {
    const item = doc.data();

    items.push({ ...item, id: doc.id });
  });

  return items;
};
