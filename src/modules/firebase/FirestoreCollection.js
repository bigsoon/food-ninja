import { cloneElement, PureComponent } from 'react';

import { db } from '.';
import { parseQuerySnapshot } from './utils';

class FirestoreCollection extends PureComponent {
  static getDerivedStateFromProps({ name }) {
    return {
      collectionRef: db.collection(name)
    };
  }
  state = { collectionRef: null, data: null, unsubscribe: () => null };

  componentWillUnmount = () => {
    this.state.unsubscribe();
  };

  componentDidMount = () => {
    const { collectionRef } = this.state;

    if (collectionRef) {
      const unsubscribe = collectionRef.onSnapshot((querySnapshot) => {
        this.setState(() => ({ data: parseQuerySnapshot(querySnapshot) }));
      });

      this.setState(() => ({ unsubscribe }));
    }
  };

  render() {
    const { data } = this.state;
    const { render, component } = this.props;

    if (!data) {
      return 'Loading...';
    }

    if (component) {
      return cloneElement(component, {
        data
      });
    }

    return render(data);
  }
}

export default FirestoreCollection;
