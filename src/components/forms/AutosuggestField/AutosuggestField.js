import React from 'react';
import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';

function renderInputComponent(inputProps) {
  const { classes, ...other } = inputProps;

  return <TextField fullWidth {...other} />;
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.name, query);
  const parts = parse(suggestion.name, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map((part, index) =>
          part.highlight ? (
            <span key={String(index)} style={{ fontWeight: 500 }}>
              {part.text}
            </span>
          ) : (
            <strong key={String(index)} style={{ fontWeight: 300 }}>
              {part.text}
            </strong>
          )
        )}
      </div>
    </MenuItem>
  );
}

function getSuggestions(suggestions, value) {
  const inputValue = deburr(value.trim()).toLowerCase();

  return suggestions.filter((suggestion) => {
    const suggestionValue = suggestion.name.toLowerCase();
    const keep = suggestionValue.includes(inputValue);

    return keep;
  });
}

function getSuggestionValue(suggestion) {
  return suggestion.name;
}

const styles = (theme) => ({
  container: {
    position: 'relative'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 999,
    left: 0,
    right: 0
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  divider: {
    height: theme.spacing.unit * 2
  }
});

class AutosuggestField extends React.Component {
  state = {
    value: '',
    suggestions: []
  };

  handleChange = (e, { newValue }) => {
    this.setState({ value: newValue });
  };

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
    }
  };

  handleSuggestionsFetchRequested = ({ value }) => {
    const { data } = this.props;

    this.setState({
      suggestions: getSuggestions(data, value)
    });
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  handleSuggestionSelected = (_, { suggestion }) => {
    const { onSelect } = this.props;

    onSelect(suggestion);
    this.setState({ value: '' });
  };

  render() {
    const { classes, label, placeholder, variant, margin } = this.props;
    const { value } = this.state;

    const autosuggestProps = {
      renderInputComponent,
      suggestions: this.state.suggestions,
      onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
      onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
      onSuggestionSelected: this.handleSuggestionSelected,
      getSuggestionValue,
      renderSuggestion,
      shouldRenderSuggestions: () => true,
      highlightFirstSuggestion: true,
      focusInputOnSuggestionClick: false
    };
    const inputProps = {
      classes,
      label,
      placeholder,
      variant,
      margin,
      value,
      onKeyPress: this.handleKeyPress,
      onChange: this.handleChange
    };

    return (
      <Autosuggest
        {...autosuggestProps}
        inputProps={inputProps}
        theme={{
          container: classes.container,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion
        }}
        renderSuggestionsContainer={(options) => (
          <Paper {...options.containerProps} square>
            {options.children}
          </Paper>
        )}
      />
    );
  }
}

AutosuggestField.defaultProps = {
  label: 'Add suggestion',
  placeholder: 'Start typing...',
  variant: 'outlined',
  margin: 'normal'
};

AutosuggestField.propTypes = {
  name: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  variant: PropTypes.string,
  margin: PropTypes.string
};

export default withStyles(styles)(AutosuggestField);
