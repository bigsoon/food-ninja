import React from 'react';
import { Formik } from 'formik';
import { func, object } from 'prop-types';

const Form = (props) => {
  return <Formik {...props} />;
};

Form.propTypes = {
  initialValues: object.isRequired,
  validationSchema: object,
  onSubmit: func.isRequired,
  render: func.isRequired
};

export default Form;
