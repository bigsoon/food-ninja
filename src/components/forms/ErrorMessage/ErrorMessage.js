import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { ErrorMessage as FormikErrorMessage } from 'formik';
import { string, object } from 'prop-types';

const styles = {
  errorMessage: {
    color: 'red'
  }
};

const ErrorMessage = ({ name, classes: { errorMessage } }) => {
  if (!name) {
    throw new Error('Name must be provided!');
  }
  return (
    <FormikErrorMessage name={name}>
      {(msg) => <div className={errorMessage}>{msg}</div>}
    </FormikErrorMessage>
  );
};

ErrorMessage.propTypes = {
  name: string.isRequired,
  classes: object.isRequired
};

export default withStyles(styles)(ErrorMessage);
