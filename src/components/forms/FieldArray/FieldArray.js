import React from 'react';
import { FieldArray as FormikFieldArray } from 'formik';
import { string, func } from 'prop-types';

const FieldArray = ({ name, render }) => {
  if (!name) {
    throw new Error('Name must be provided!');
  }

  return <FormikFieldArray name={name} render={render} />;
};

FieldArray.propTypes = {
  name: string.isRequired,
  render: func.isRequired
};

export default FieldArray;
