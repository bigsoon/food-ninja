import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Form as FormikForm } from 'formik';
import { array, object } from 'prop-types';

const styles = (theme) => ({
  form: {
    margin: theme.spacing.unit * 3,
    width: '500px'
  }
});

const Form = ({ classes: { form }, ...restProps }) => {
  return <FormikForm className={form} {...restProps} />;
};

Form.propTypes = {
  classes: object.isRequired,
  children: array.isRequired
};

export default withStyles(styles, { withTheme: true })(Form);
