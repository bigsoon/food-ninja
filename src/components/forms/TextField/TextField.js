import React from 'react';
import { Field } from 'formik';
import { TextField as MuiTextField } from '@material-ui/core';
import { string, bool } from 'prop-types';

const TextField = ({ name, ...restProps }) => {
  if (!name) {
    throw new Error('Name must be provided');
  }

  return (
    <>
      <Field
        name={name}
        render={({ field }) => <MuiTextField {...field} {...restProps} />}
      />
    </>
  );
};

TextField.defaultProps = {
  fullWidth: false,
  label: 'Label',
  placeholder: 'Placeholder',
  variant: 'outlined',
  margin: 'normal'
};

TextField.propTypes = {
  name: string.isRequired,
  fullWidth: bool,
  label: string,
  placeholder: string,
  variant: string,
  margin: string
};

export default TextField;
