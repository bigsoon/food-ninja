import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';

import Routes from './Routes';

import 'typeface-roboto';
import './base.css';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});

export const App = () => (
  <BrowserRouter>
    <SnackbarProvider maxSnack={5}>
      <MuiThemeProvider theme={theme}>
        <>
          <Routes />
          <CssBaseline />
        </>
      </MuiThemeProvider>
    </SnackbarProvider>
  </BrowserRouter>
);

export default App;
