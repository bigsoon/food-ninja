import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Dashboard from '../modules/dasbhoard';

export const Routes = () => (
  <Switch>
    <Route path="/dashboard" component={Dashboard} />
    <Redirect to="/dashboard" />
  </Switch>
);

export default Routes;
